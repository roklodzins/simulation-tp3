/**
 * @file experiments.c/h
 * @brief implementation of the experiments functions
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */


////////////////////////  Includes  ///////////////////////////

#include "experiments.h"
#include "utils.h"
#include "mt.h"
#include <math.h>
#include <pthread.h>


/////////////////////   Declarations   ////////////////////////


static const double STUDENT[] = { // Student law coeff (99% confidence), for k in : [0-30] U {40, 50, 60, 80, 100, 120, inf}
        INFINITY, 63.66, 9.925, 5.841, 4.604, 4.032, 3.707, 3.499, 3.355, 3.25, 3.169, 3.106, 3.055, 3.012, 2.977, 2.947, 2.921, 2.898, 2.878, 2.861, 2.845, 2.831, 2.819, 2.807, 2.797, 2.787, 2.779, 2.771, 2.763, 2.756, 2.75, 2.704, 2.678, 2.66, 2.639, 2.626, 2.617, 2.576
};

static int nb_points_thread; // used be replicatePiTh & simPiTh


///////////////////////   Functions   /////////////////////////

/**
 * @brief estimate π using Monte Carlo method with Mersenne Twister
 * @param[in] nb_points the number of points to draw
 * @return an estimate of π
 * */
double simPi(int nb_points) {
	int in = 0;

	for (int i = 0; i < nb_points; i++) {
		
		double x = genrand_real1(),
		       y = genrand_real1();
		
		if (x * x + y * y <= 1) in++;
	}

	return 4. * (double)in / (double)nb_points;
}


/**
 * @brief print the result of a set of replicated experiments including mean, variance,
 * standard deviation, error against π, elapsed time and confidence interval at 99%
 * @param[in] mean the mean of the set
 * @param[in] variance the variance of the set
 * @param[in] replicates the number of replicates
 * @note for the elapsed time, time_it() must be call before
 * */
static inline void printResult(double mean, double variance, int replicates) {
	
	double ub_variance = replicates * variance / (replicates - 1), // unbiased variance
		   error = M_PI - mean,
		   R = sqrt(ub_variance / replicates),
		   location;
	
	if (replicates <= 30) R *= STUDENT[replicates];                // replicates=0 will cause R=inf -> interval = read number -> ok
	else if (replicates <= 60) R *= STUDENT[27 + replicates / 10]; // dispatch 10-sized intervals
	else if (replicates < 140) R *= STUDENT[30 + replicates / 20]; // dispatch 20-sized intervals
	else R *= STUDENT[37];                                         // infinite

	location = (error + R) * 100. / R;
	if (location > 100.) location = 200. - location;
	
	H3("\t- Time :                      \t" RST "%.10f seconds\n", time_it());
	H3("\t- Mean :                      \t" RST "%.10f\n", mean);
	H3("\t- Variance :                  \t" RST "%.10f\n", variance);
	H3("\t- Unbiased variance :         \t" RST "%.10f\n", ub_variance);
	H3("\t- Standard deviation :        \t" RST "%.10f\n", sqrt(variance));
	H3("\t- Absolute error : π - mean : \t" RST "%.10f\n", error);
	H3("\t- Relative error : Err / π :  \t" RST "%.10f %%\n", 100 * error / M_PI);
	H3("\t- Standard error :            \t" RST "%.10f\n", sqrt(variance / replicates)); // stddev / sqrt(n)
	H3("\t- Confidence interval :       \t" RST "[ %.10f ; %.10f ]\n", mean - R, mean + R);
	H3("\t- π location in interval :    \t" RST "%.10f %%\n", location); // distance to the center: 100% = center, 0% = limit, -X% = out of interval
	H3("\t- Confidence radius :         \t" RST "%.10f\n\n", R);
}


/**
 * @brief replicate the experiment of estimating π
 * @param[in] nb_points the number of points to draw (passed to simPi)
 * @param[in] replicates the number of replicates requested = number of independant simulations
 * */
void replicatePi(int nb_points, int replicates) {
	double mean = 0, smean = 0; // smean = mean of squares

	H2("Estimation of π with %d points and %d replicates :\n", nb_points, replicates);
	time_it();
	
	for (int i = 0; i < replicates; i++){
		double pi = simPi(nb_points);
		mean += pi;
		smean += pi * pi;
	}

	mean  /= replicates;
	smean /= replicates;

	printResult(mean, smean - mean * mean, replicates);
}


/**
 * @brief Thread wrapper for simPi function
 * @param[out] arg destination of the output of simPi (double)
 * @return NULL
 * @note nb_points argument of simPi is passed by the nb_points_thread static global variable
 * */
static inline void * simPiTh(void * arg) {
	*(double *)arg = simPi(nb_points_thread);
	return NULL;
}


/**
 * @brief Thread version of replicatePi, where each replicate is done in its own thread
 * @param[in] nb_points the number of points to draw (passed to simPi)
 * @param[in] replicates the number of replicates requested = the number of thread (max = 255)
 * @note thread creation test are not performed
 * @note threads accesses to MT are not mutexed (seams to be ok) for performance purpose (still worse than replicatePi)
 * */
void replicatePiTh(int nb_points, unsigned char replicates) {
	double mean = 0,
	       smean = 0;
	
	pthread_t thread[replicates]; // u8 -> safe VLA, malloc not needed
	double pis[replicates];
	
	nb_points_thread = nb_points;

	H2("Estimation of π with %d points and %d iterations :\n", nb_points, replicates);
	time_it();
	
	for (int i = 0; i < replicates; i++) pthread_create(thread + i, NULL, simPiTh, pis + i);
	
	for (int i = 0; i < replicates; i++) {
		pthread_join(thread[i], NULL);
		mean += pis[i];
		smean += pis[i] * pis[i];
	}

	mean  /= replicates;
	smean /= replicates;
	
	printResult(mean, smean - mean * mean, replicates);
}
