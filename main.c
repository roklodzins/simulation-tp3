/**
 * @file main.c
 * @brief Simulation TP3 - F2 ZZ2
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */


////////////////////////  Includes  ///////////////////////////

#include <stdio.h>
#include "experiments.h"
#include "mt.h"
#include "utils.h"


////////////////////////    Main    ///////////////////////////

/**
 * @brief the main program, Simulation TP3 - F2 ZZ2
 * */
int main(void) {
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456};
	
	int replicates = 0,
	    nb_points = 0,
		answer = 0;

	
    H1("Simulation F2 ZZ2 - TP3 : Romain KLODZINSKI (c) 2023\n\n");

	
    ////////////////////////  MT Test  ///////////////////////////

	int test = mt_test();
	if (test == -1) H4("Mersenne Twister test OK\n\n\n");
	else H1("Mersenne Twister test FAILED at i = %d, please report the error\n\n\n", test);
	
    init_by_array(init, 4); // MT init
	
	
    ////////////////////////  Question 1  ///////////////////////////

	H1("Question 1 - Estimate π\n\n\n");
	
	Ask(answer, "Pass this section ? (0 = No | 1 = Yes) : ");
	
	if (!answer) {
		H2("\nEstimation of π with :\n");
		time_it();
		
		H3("\t - 1 000 points : \t\t"       RST "%.10f ", simPi(1000));
		printf("(in %.10f seconds)\n", time_it()); // using time_it in the H3 cause simPi to be executed after it = bad time (C's undefined behavior)

		H3("\t - 1 000 000 points : \t\t"   RST "%.10f ", simPi(1000000));
		printf("(in %.10f seconds)\n", time_it());
		
		H3("\t - 1 000 000 000 points : \t" RST "%.10f ", simPi(1000000000));
		printf("(in %.10f seconds)\n\n\n", time_it());
		
	}
    
	
    ////////////////////////  Question 2  ///////////////////////////

	H1("\n\nQuestion 2 - Replicated estimates of π\n\n");

	Ask(answer, "Pass this section ? (0 = No | 1 = Yes) : ");
	
	if (!answer) {
	    replicatePi(1000, 10);
	    replicatePi(1000000, 10);
	    replicatePi(1000000000, 10);
		
	    replicatePi(1000, 30);
	    replicatePi(1000000, 30);
	    replicatePi(1000000000, 30);
	}
	

    ////////////////////////  Question 3  ///////////////////////////

	H1("\n\nQuestion 3 - Replicated estimates of π - user mode\n\n");

	Ask(answer, "Pass this section ? (0 = No | 1 = Yes) : ");
	answer = !answer;
	
	while (answer) {
        AskUntil(nb_points, nb_points <= 0, "Number of points (n > 0) :");
        AskUntil(replicates, replicates <= 1, "Number of replicates (n > 1) :");
		replicatePi(nb_points, replicates);
		
		Ask(answer, "Retry ? (0 = No | 1 = Yes) : ");
	}
	
	
    //////////////////////  Question 3 Exp  /////////////////////////

	H1("\n\nQuestion 3 Experiments - Replicated estimates of π with fixed settings\n\n");
	
	Ask(answer, "Pass this section / exit ? (0 = No | 1 = Yes) : ");
	
	if (!answer) {
		H1("Experiments with fixed total draws\n\n");
		replicatePi(1000000000, 10);
		replicatePi(1000000, 10000);
		replicatePi(1000, 10000000);
		
		H1("\nExperiments with fixed number of points\n\n");
		replicatePi(1000, 10);
		replicatePi(1000, 10000);
		replicatePi(1000, 10000000);
		
		H1("\nExperiments with fixed number of replicates\n\n");
		replicatePi(1000000000, 10);
		replicatePi(1000000, 10);
		replicatePi(1000, 10);
	}
	
	H4("\n\nExit success\n");
	return 0;
}

