cmake_minimum_required(VERSION 3.20) # abitrary minimal version, can be lowered if needed
project(TP3 C)

set(CMAKE_C_STANDARD 23)      # standard C23
set(CMAKE_VERBOSE_MAKEFILE TRUE)

## Libs
add_compile_options("-lm")    # link math lib

## Optimizations
#add_compile_options("-O3")           # optimize
add_compile_options("-Ofast")         # hard optimize
add_compile_options("-fno-fast-math") # cancel Ofast math break (remove -f-fast-math)
add_compile_options("-flto")          # linktime optimize
add_compile_options("-march=native")  # current CPU arch optimize

## Warnings
add_compile_options("-Wall")
add_compile_options("-Wextra")
add_compile_options("-Wno-unused-result") # remove scanf useless warning
add_compile_options("-pedantic")

## Build
add_executable(TP3 main.c
        src/mt.c
        src/utils.c
        src/experiments.c
)

include_directories("include")       # add the include dir to gcc path
target_link_libraries(TP3 PRIVATE m) # link math lib
