/**
 * @file mt.c/h
 * @brief implementation of the Mersenne Twister
 * @copyright Makoto Matsumoto & Takuji Nishimura
 * */

#ifndef TP3_MT_H
#define TP3_MT_H


///////////////////////   Functions   /////////////////////////

void init_genrand(unsigned long s);
void init_by_array(unsigned long init_key[], int key_length);

unsigned long genrand_int32(void);
long genrand_int31(void);

double genrand_real1(void);
double genrand_real2(void);
double genrand_real3(void);
double genrand_res53(void);

/**
 * @brief test the first 2000 numbers as integers and reals, comparing to hardcoded raw values tables
 * @note possibility to verify this properly work by simply corrupting a value
 * in int_values or real_values in mt.c, causing the test to successfully fail with a working MT
 * */
int mt_test();

#endif //TP3_MT_H

