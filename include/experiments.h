/**
 * @file experiments.c/h
 * @brief implementation of the experiments functions
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */


#ifndef EXPERIMENTS_H
#define EXPERIMENTS_H


///////////////////////   Functions   /////////////////////////

/**
 * @brief estimate π using Monte Carlo method with Mersenne Twister
 * @param[in] nb_points the number of points to draw
 * @return an estimate of π
 * */
double simPi(int iter);


/**
 * @brief replicate the experiment of estimating π
 * @param[in] nb_points the number of points to draw (passed to simPi)
 * @param[in] replicates the number of replicates requested = number of independant simulations
 * */
void replicatePi(int nb_points, int replicates);


/**
 * @brief Thread version of replicatePi, where each replicate is done in its own thread
 * @param[in] nb_points the number of points to draw (passed to simPi)
 * @param[in] replicates the number of replicates requested = the number of thread (max = 255)
 * @note thread creation test are not performed
 * @note threads accesses to MT are not mutexed (seams to be ok) for performance purpose (still worse than replicatePi)
 * */
void replicatePiTh(int nb_points, unsigned char replicates);

#endif //EXPERIMENTS_H
